<section class="bg-gray-100 py-20">
	<div class="container">
		<h1 class="text-3xl font-bold text-gray-800">
			{!! \App\page_title() !!}
		</h1>
	</div>
</section>
