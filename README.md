# WP Kickoff

**This theme is still in active development. We do not recommend to use it in production yet.**

## Credits

A lot of this theme is inspired by the Sage theme (by Roots).

## Installation

In `webpack.mix.js`, change the `proxy` variable, and update the `PATH_TO_THEME` constant.

Install composer dependencies with `composer install`.

Install NPM dependencies with `npm i`.

## Building assets

```
npm run dev
npm run watch
npm run production
```

## Troubleshooting / FAQ

### Linking to images

In CSS, use relative paths:

```
html {
    background-image: url('../images/background.png');
}
```

In your Blade file, link to them using the `mix` helper function:

```
<img src="{{ mix('images/logo.svg') }}" alt="Logo" />
```
