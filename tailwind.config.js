const colors = require('tailwindcss/colors')

module.exports = {
	purge: ['./**/*.blade.php', './**/*.scss'],
	darkMode: false,
	theme: {
		extend: {
			colors: {
				gray: colors.coolGray,
				red: colors.rose,
				green: colors.emerald,
				primary: colors.sky,
			},
		},
		container: {
			center: true,
			padding: '1rem',
		},
		fontFamily: {
			body: ['"Nunito", sans-serif'],
		},
	},
	variants: {},
	plugins: [
		function ({ addComponents }) {
			addComponents({
				'.container': {
					maxWidth: '100%',
					'@screen sm': {
						maxWidth: '600px',
					},
					'@screen md': {
						maxWidth: '760px',
					},
					'@screen lg': {
						maxWidth: '960px',
					},
					'@screen xl': {
						maxWidth: '1160px',
					},
				},
			})
		},
	],
}
