<!DOCTYPE html>
<html lang="{{ get_locale() }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>

	@php
		wp_head();
	@endphp
</head>
<body class="text-gray-700 antialiased text-base leading-relaxed font-body">
	@include('partials.header')

	<main>
		@yield('content')
	</main>

	@include('partials.footer')

	@php
		wp_footer();
	@endphp
</body>
</html>
