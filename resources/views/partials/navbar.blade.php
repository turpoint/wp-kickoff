<nav class="bg-primary-900 py-2">
	<div class="container flex items-center justify-between">
		<a href="{{ get_home_url() }}" aria-label="{{ __('Back to home', 'kickoff') }}">
			<img
				src="{{ mix('images/logo.svg') }}"
				alt="Logo {{ get_bloginfo('title') }}"
				class="h-12"
			/>
		</a>


		@php
			wp_nav_menu([
				'theme_location' => 'primary',
				'menu_class' => 'nav-primary'
			])
		@endphp
	</div>
</nav>
