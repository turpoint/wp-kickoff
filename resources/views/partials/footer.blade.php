
<footer>
	<!-- Prefooter -->
	<section class="md:py-24 py-16 bg-primary-800">
		<div class="container">
			<div class="flex flex-wrap -mx-4 -mb-12">
				@php
					dynamic_sidebar('footer');
				@endphp
			</div>
		</div>
	</section>
	<!-- End Prefooter -->

	<!-- Copyright -->
	<section class="py-8 bg-primary-900">
		<div class="container text-primary-200 text-center">
			<span>
				Copyright {{ \Date('Y') }}
				{{ get_bloginfo('title') }}
			</span>
		</div>
	</section>
	<!-- End Copyright -->
</footer>
