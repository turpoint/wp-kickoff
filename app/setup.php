<?php

namespace App;

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function() {
    wp_enqueue_style('kickoff/app.css', mix('app.css'), false, null);
    wp_enqueue_script('kickoff/app.js', mix('app.js'), [], null, true);
}, 100);

/**
 * Theme support
 */
add_action('after_setup_theme', function() {

	/**
	 * Register navigation menus
	 */
	register_nav_menus([
        'primary' => __('Primary', 'kickoff')
    ]);

	/**
	 * Enabling post thumbnails
	 */
	add_theme_support('post-thumbnails');

	/**
	 * Adding custom image sizes
	 */
	add_image_size('product_thumbnail', 600, 600, true);
	add_image_size('product_thumbnail_large', 1000, 1000, true);
});


/**
 * Widgets
 */
add_action('widgets_init', function() {
    register_sidebar([
        'name' => __('Footer', 'kickoff'),
        'id' => 'footer',
		'before_widget' => '<div class="px-4 md:w-1/4 sm:w-1/2 w-full footer-block mb-12">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ]);

	register_sidebar([
        'name' => __('Sidebar', 'kickoff'),
        'id' => 'sidebar',
		'before_widget' => '<div class="sidebar-block mb-4">',
        'after_widget' => '</div>',
        'before_title' => '<h4>',
        'after_title' => '</h4>'
    ]);
});
