@include('partials.page-header')

<section class="py-24">
	<div class="container">
		<div class="post-content">
			{!! the_content() !!}
		</div>
	</div>
</section>
