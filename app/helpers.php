<?php

namespace App;

/**
 * Page title
 */
function page_title()
{
	if (is_home()) {
		if ($home = get_option('page_for_posts', true)) {
			return get_the_title($home);
		}
		return __('Latest Posts', 'kickoff');
	}
	if (is_archive()) {
		return get_the_archive_title();
	}
	if (is_search()) {
		return sprintf(__('Search Results for %s', 'kickoff'), get_search_query());
	}
	if (is_404()) {
		return __('Not Found', 'kickoff');
	}
	return get_the_title();
}
