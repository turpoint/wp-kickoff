@include('partials.page-header')

<section class="py-24">
	<div class="container">
		<div class="-mx-4 flex flex-wrap -mb-12">
			@while (have_posts()) @php the_post() @endphp
			<div class="px-4 mb-12 w-1/2">
				@include('partials.preview-'.get_post_type())
			</div>
			@endwhile
		</div>
	</div>
</section>
