<h4 class="font-bold text-xl mb-3">
	<a href="{{ get_the_permalink() }}" class="text-gray-800 hover:text-primary-700 hover:underline">
		{!! get_the_title() !!}
	</a>
</h4>

{!! wpautop(get_the_excerpt()) !!}
