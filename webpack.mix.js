const mix = require('laravel-mix')
const tailwindcss = require('tailwindcss')

require('laravel-mix-imagemin')
require('laravel-mix-svelte');

const PATH_TO_THEME = '/app/themes/kickoff/dist/'
const DIST_DIR = 'dist/'

mix.setPublicPath(DIST_DIR)
	.js('resources/scripts/app.js', DIST_DIR)
	.svelte({
        dev: true
    })
	.sass('resources/styles/app.scss', DIST_DIR)
	.setResourceRoot(`${PATH_TO_THEME}${DIST_DIR}`)
	.options({
		processCssUrls: false,
		postCss: [tailwindcss('./tailwind.config.js')],
	})
	.copy('resources/images/*.*', 'dist/images')
	.version()

mix.browserSync({
	files: ['dist/**/*.js', 'dist/**/*.css', 'resources/views/**/*.blade.php'],
	proxy: 'kickoff.test',
})
